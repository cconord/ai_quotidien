# Lien à cliquer
[Le lien vers le site de démarrage de l'atelier AI Quotidien](https://ai-quotidien-cconord-3405cf7fa04bbb9ea6509ca1f3ccdbc28079a3eb1a.gitpages.huma-num.fr/)

# Lien à recopier durant le TP (url réduite via Huma-Num URL)
https://page.hn/vx5e4n

# Lien à zieuter (avec la caméra d'un smartphone par ex)
![QRcode créé avec Duckduckgo +qrcode url](qrcode.png)